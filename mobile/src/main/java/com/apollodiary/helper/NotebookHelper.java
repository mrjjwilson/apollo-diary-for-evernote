package com.apollodiary.helper;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.apollodiary.ApolloDiaryApplication;

public class NotebookHelper {

    private static final String LOGTAG = NotebookHelper.class.getSimpleName();
    private static final String DEFAULT_NOTEBOOK_GUID = "defaultNotebookGuid";
    private static final String DEFAULT_NOTEBOOK_NAME = "defaultNotebookName";

    public static String getDefaultNotebookName() {
        return getSharedPreferencesString(DEFAULT_NOTEBOOK_NAME, null);
    }

    public static void setDefaultNotebookName(String defaultNotebookName) {
        setSharedPreferencesString(DEFAULT_NOTEBOOK_NAME, defaultNotebookName);
    }


    public static String getDefaultNotebookGuid() {
        return getSharedPreferencesString(DEFAULT_NOTEBOOK_GUID, null);
    }

    public static void setDefaultNotebookGuid(String defaultNotebookGuid) {
        setSharedPreferencesString(DEFAULT_NOTEBOOK_GUID, defaultNotebookGuid);
    }

    public static void deleteDefaultNotebookName() {
        setSharedPreferencesString(DEFAULT_NOTEBOOK_NAME, null);
    }

    public static void deleteDefaultNotebookGuid() {
        setSharedPreferencesString(DEFAULT_NOTEBOOK_GUID, null);
    }

    public static boolean isDefaultNotebookSet() {
//        return getDefaultNotebookGuid() != null && getDefaultNotebookName() != null ;
        return getDefaultNotebookName() != null;
    }

    private static void setSharedPreferencesString(String key, String value) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ApolloDiaryApplication.getInstance());
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.commit();
    }

    private static String getSharedPreferencesString(String storedString, String defaultValue ) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(ApolloDiaryApplication.getInstance());
        return settings.getString(storedString, defaultValue);
    }
}
