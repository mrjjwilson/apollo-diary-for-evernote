package com.apollodiary.helper;

import com.evernote.edam.type.Note;

public class NoteHelper {

    private static final String LOGTAG = NoteHelper.class.getSimpleName();

    public static boolean isNoteValid(Note note) {
        boolean validTitle = note.getTitle() != null;
        boolean validContent = note.getContent() != null;
        return (validTitle && validContent);
    }
}
