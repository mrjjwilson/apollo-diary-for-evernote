package com.apollodiary;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.evernote.client.android.EvernoteSession;
import com.flurry.android.FlurryAgent;
import com.path.android.jobqueue.JobManager;

import de.greenrobot.event.EventBus;
import io.fabric.sdk.android.Fabric;

public class ApolloDiaryApplication extends Application {


    // Your Evernote API key. See http://dev.evernote.com/documentation/cloud/
    // Please obfuscate your code to help keep these values secret.
    private static final String CONSUMER_KEY = "mrjjwilson-0133";
    private static final String CONSUMER_SECRET = "5e66b27a735a7824";

    // Initial development is done on Evernote's testing service, the sandbox.
    // Change to HOST_PRODUCTION to use the Evernote production service
    // once your code is complete, or HOST_CHINA to use the Yinxiang Biji
    // (Evernote China) production service.
    private static final EvernoteSession.EvernoteService EVERNOTE_SERVICE = EvernoteSession.EvernoteService.PRODUCTION;

    private static final boolean SUPPORT_APP_LINKED_NOTEBOOKS = true;
    private static final String MY_FLURRY_APIKEY = "BN3VKXRVX9TNJQKFKQ2C";


    private static ApolloDiaryApplication application;
    private static EvernoteSession evernoteSession;
    private static JobManager jobManager;

    public static EventBus getEventBus() {
        return EventBus.getDefault();
    }

    public static ApolloDiaryApplication getInstance() {
        return application;
    }

    public static EvernoteSession getEvernoteSession() {
        return evernoteSession;
    }

    public static JobManager getJobManager() {
        return jobManager;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        application = this;
        evernoteSession = EvernoteSession.getInstance(this, CONSUMER_KEY, CONSUMER_SECRET, EVERNOTE_SERVICE, SUPPORT_APP_LINKED_NOTEBOOKS);
        jobManager = new JobManager(this);
        FlurryAgent.setLogEnabled(false);
        FlurryAgent.init(this, MY_FLURRY_APIKEY);
    }
}
