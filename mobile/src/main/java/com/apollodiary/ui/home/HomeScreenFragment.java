package com.apollodiary.ui.home;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.apollodiary.ApolloDiaryApplication;
import com.apollodiary.R;
import com.apollodiary.event.ConnectionErrorEvent;
import com.apollodiary.event.DeletedNoteEvent;
import com.apollodiary.event.FetchedAllNotesEvent;
import com.apollodiary.event.NoteCreatedEvent;
import com.apollodiary.event.NoteUpdatedEvent;
import com.apollodiary.event.NotebookCreatedEvent;
import com.apollodiary.event.NotebookErrorEvent;
import com.apollodiary.event.NotesNotFoundEvent;
import com.apollodiary.helper.NotebookHelper;
import com.apollodiary.job.CreateNotebookJob;
import com.apollodiary.job.FetchAllNotesJob;
import com.apollodiary.ui.BaseEventBusFragment;
import com.apollodiary.ui.note.NoteActivity;
import com.apollodiary.ui.note.NoteAdapter;
import com.apollodiary.ui.note.NoteType;
import com.apollodiary.utils.ConstantStrings;
import com.evernote.client.android.EvernoteSession;
import com.evernote.edam.notestore.NoteMetadata;
import com.path.android.jobqueue.JobManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.Optional;
import de.greenrobot.event.EventBus;

public class HomeScreenFragment extends BaseEventBusFragment {


    private static final String LOGTAG = HomeScreenFragment.class.getSimpleName();

    @InjectView(R.id.home_progress_bar)
    @Optional
    ProgressBar homeProgressBar;

    @InjectView(R.id.empty_listview)
    @Optional
    TextView empty_listview;

    @InjectView(R.id.no_notes_found)
    @Optional
    TextView no_notes_found_textview;

    @Optional
    @InjectView(R.id.connection_error)
    TextView connection_error_textview;

    @Optional
    @InjectView(R.id.create_notebook_button)
    Button create_notebook_button;

    @Optional
    @InjectView(R.id.notebook_missing_textview)
    TextView notebook_missing_textview;

    @Optional
    @InjectView(R.id.sync_icon)
    ImageButton sync_icon;

    private ArrayList<NoteMetadata> noteMetadataList;
    private AbsListView mListView;
    private ArrayAdapter adapter;
    private JobManager jobManager;
    private EvernoteSession evernoteSession;

    public HomeScreenFragment() {
    }

    public static HomeScreenFragment newInstance() {
        HomeScreenFragment fragment = new HomeScreenFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        evernoteSession = ApolloDiaryApplication.getEvernoteSession();
        jobManager = ApolloDiaryApplication.getJobManager();
        noteMetadataList = new ArrayList<NoteMetadata>();
        adapter = new NoteAdapter(getActivity(), R.layout.home_listview_item_row, noteMetadataList);
        storeSetupData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item, container, false);
        ButterKnife.inject(this, view);

        mListView = (ListView) view.findViewById(android.R.id.list);
        mListView.setAdapter(adapter);
        mListView.setEmptyView(empty_listview);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                NoteMetadata noteMetadata = (NoteMetadata) adapter.getItem(position);
                String noteGuid = noteMetadata.getGuid();
                openExistingNote(noteGuid);
            }
        });

        setHasOptionsMenu(true);

        fetchAllNotes();

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        checkEventBusNoteUpdates();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id;
        id = item.getItemId();
        switch (id){
            case R.id.sync_icon_actionbar:
                checkNoteUpdates();

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void storeSetupData(){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(ConstantStrings.SHOW_SETUP, false);
        editor.apply();
    }

    /**
     * Note Handling
     */
    private void fetchAllNotes() {
        if(NotebookHelper.getDefaultNotebookGuid() != null) {
            refreshAdapter();
            refreshEmptyView(homeProgressBar);
            toggleView(homeProgressBar, View.VISIBLE);
            jobManager.addJobInBackground(new FetchAllNotesJob(NotebookHelper.getDefaultNotebookGuid()));
        } else {
            showToastShort("WOOPS!!, defaultNotebookGuid not set");
        }
    }

    @SuppressWarnings("unused")
    @Optional
    @OnClick(R.id.sync_icon)
    public void syncNotes() {
        jobManager.addJobInBackground(new FetchAllNotesJob(NotebookHelper.getDefaultNotebookGuid()));
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.fab_new_note)
    public void openNewNote() {
        Intent i = new Intent(getActivity(), NoteActivity.class);
        i.putExtra(ConstantStrings.NOTE_TYPE, NoteType.CREATE_NOTE);
        startActivity(i);
    }


    private void openExistingNote(String noteGuid) {
        Intent i = new Intent(getActivity(), NoteActivity.class);
        i.putExtra(ConstantStrings.NOTE_TYPE, NoteType.EXISTING_NOTE);
        i.putExtra(ConstantStrings.NOTE_GUID, noteGuid);
        startActivity(i);
    }

    /**
     * Modify Adapter & ListView
     */
    private void refreshAdapter() {
        adapter.clear();
        adapter.notifyDataSetChanged();
    }


    private void refreshEmptyView(View view) {
        mListView.getEmptyView().setVisibility(View.GONE);
        mListView.setEmptyView(view);
    }

    /**
     * Modify Views
     */
    private void hideErrorViews() {
        toggleViews(Arrays.asList(connection_error_textview, sync_icon, notebook_missing_textview, create_notebook_button), View.GONE);
    }

    private void toggleView(View view, int visibility) {
        toggleViews(Arrays.asList(view), visibility);
    }


    private void toggleViews(List<View> views, int visibility) {
        for (View view : views) {

            switch (visibility) {

                case View.GONE:
                case View.INVISIBLE:
                    if (view != null && view.getVisibility() == View.VISIBLE) {
                        view.setVisibility(visibility);
                    }
                    break;

                case View.VISIBLE:
                    if (view != null) {
                        view.setVisibility(View.VISIBLE);
                    }
                    break;

                default:
                    throw new IllegalArgumentException("Unsupported visibility type" + visibility);
            }
        }
    }

    @SuppressWarnings("unused")
    @OnClick(R.id.create_notebook_button)
    public void createNotebook() {
        jobManager.addJobInBackground(new CreateNotebookJob(evernoteSession, ConstantStrings.APOLLO_DIARY));
        showToastShort("Creating Notebook");
    }

    private void showToastShort(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
    }

    /**
     * EventBus Sticky Events
     */
    private void checkNoteUpdates() {
        fetchAllNotes();
        if(isNoteCreatedStickyEventFound()) {
            EventBus.getDefault().removeStickyEvent(NoteCreatedEvent.class);
        }
    }

    private void checkEventBusNoteUpdates() {
        if(isNoteCreatedStickyEventFound()) {
            refreshNotesFromStickyEvent(NoteCreatedEvent.class);
        }

        if(isNoteUpdatedStickEventFound()) {
            refreshNotesFromStickyEvent(NoteUpdatedEvent.class);
        }

        if(isDeletedNoteStickyEventFound()) {
            refreshNotesFromStickyEvent(DeletedNoteEvent.class);
        }
    }

    private void refreshNotesFromStickyEvent(Class<?> eventType) {
        fetchAllNotes();
        EventBus.getDefault().removeStickyEvent(eventType);
    }

    private boolean isDeletedNoteStickyEventFound() {
        return isStickyEventFound(DeletedNoteEvent.class);
    }

    private boolean isNoteUpdatedStickEventFound() {
        return isStickyEventFound(NoteUpdatedEvent.class);
    }

    private boolean isNoteCreatedStickyEventFound() {
        return isStickyEventFound(NoteCreatedEvent.class);
    }

    private boolean isStickyEventFound(Class<?> eventType) {
        return EventBus.getDefault().getStickyEvent(eventType) != null;
    }

    /**
     * EventBus handling methods
     */

    public void onEventMainThread(FetchedAllNotesEvent fetchedAllNotesEvent) {
        noteMetadataList.clear();
        noteMetadataList.addAll(fetchedAllNotesEvent.getAllNotes());
        toggleView(homeProgressBar, View.GONE);
        hideErrorViews();
        adapter.notifyDataSetChanged();
    }

    public void onEventMainThread(NotebookCreatedEvent notebookCreatedEvent) {
        showToastShort("Notebook Created");
        jobManager.addJobInBackground(new FetchAllNotesJob(notebookCreatedEvent.getNotebookGuid()));
    }

    public void onEventMainThread(ConnectionErrorEvent connectionErrorEvent) {
        toggleView(homeProgressBar, View.GONE);
        toggleView(sync_icon, View.VISIBLE);
        refreshEmptyView(connection_error_textview);
        refreshAdapter();
    }

    public void onEventMainThread(NotesNotFoundEvent notesNotFoundEvent) {
        toggleView(homeProgressBar, View.GONE);
        hideErrorViews();
        refreshEmptyView(no_notes_found_textview);
        refreshAdapter();
    }

    public void onEventMainThread(NotebookErrorEvent notebookErrorEvent) {
        if(notebookErrorEvent.isNotebookMissing()) {

            toggleView(homeProgressBar, View.GONE);
            toggleView(notebook_missing_textview, View.VISIBLE);
            create_notebook_button.setVisibility(View.VISIBLE);
        }

    }

}
