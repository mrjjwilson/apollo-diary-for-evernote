package com.apollodiary.ui.home;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.apollodiary.ApolloDiaryApplication;
import com.apollodiary.EvernoteActivity;
import com.apollodiary.R;
import com.evernote.client.android.EvernoteSession;
import com.path.android.jobqueue.JobManager;

import butterknife.ButterKnife;


public class HomeScreenActivity extends EvernoteActivity {

    EvernoteSession evernoteSession;
    ApolloDiaryApplication application;
    JobManager jobManager;

    @Override
    protected void setEventBusRegister() {
        isNeedRegister = false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        evernoteSession = ApolloDiaryApplication.getEvernoteSession();
        application = ApolloDiaryApplication.getInstance();
        jobManager = ApolloDiaryApplication.getJobManager();



        setContentView(R.layout.activity_home_screen);
        ButterKnife.inject(this);
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.home_screen_container, new HomeScreenFragment())
                    .commit();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id;
        id = item.getItemId();
        switch (id){
            //TODO: JERRY - Add a settings options screen -> Choose note style, sync frequency, etc...
//            case R.id.action_settings:
//                break;

            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }



}
