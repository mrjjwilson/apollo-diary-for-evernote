package com.apollodiary.ui.welcome;


import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.apollodiary.R;
import com.apollodiary.helper.NetworkHelper;
import com.apollodiary.utils.ConstantStrings;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;

public class WelcomeScreenFragment extends Fragment {

    @Optional
    @InjectView(R.id.welcome_screen_login_button)
    Button login_button;

    @Optional
    @InjectView(R.id.welcome_screen_authentication_message)
    TextView authentication_message;

    @Optional
    @InjectView(R.id.connection_error)
    TextView connection_error;

    private NetworkHelper networkHelper;

    public static WelcomeScreenFragment newInstance(){
        return new WelcomeScreenFragment();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_welcome_screen,container,false);
        networkHelper = new NetworkHelper();

        ButterKnife.inject(this, rootView);

        if(!networkHelper.isNetworkOnline()) {
            login_button.setVisibility(View.GONE);
            authentication_message.setVisibility(View.GONE);
            connection_error.setVisibility(View.VISIBLE);
        }

        return rootView;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view,savedInstanceState);

        storeFirstLaunchData();
    }


    private void storeFirstLaunchData(){
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(ConstantStrings.SHOW_WELCOME, false);
        editor.apply();
    }
}