package com.apollodiary.ui.welcome;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;

import com.apollodiary.ApolloDiaryApplication;
import com.apollodiary.EvernoteActivity;
import com.apollodiary.R;
import com.apollodiary.ui.setup.SetupActivity;
import com.apollodiary.utils.ConstantStrings;
import com.evernote.client.android.EvernoteSession;

public class WelcomeScreenActivity extends EvernoteActivity {

    @Override
    protected void setEventBusRegister() {
        isNeedRegister = false;
    }

    @Override
    public void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);

        if(getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        if(!isFirstLaunch() && isAuthenticated()){
            showSetupScreen();
        }


        setContentView(R.layout.activity_welcome_screen);

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        WelcomeScreenFragment welcomeScreenFragment = WelcomeScreenFragment.newInstance();
        ft.add(R.id.welcome_screen_container, welcomeScreenFragment);
        ft.commit();
    }

    public void login(View view){
        ApolloDiaryApplication.getEvernoteSession().authenticate(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            //Update UI when oauth activity returns result
            case EvernoteSession.REQUEST_CODE_OAUTH:
                if (resultCode == Activity.RESULT_OK) {
                    showSetupScreen();
                }
                break;
        }
    }

    private boolean isAuthenticated() {
        return ApolloDiaryApplication.getEvernoteSession().isLoggedIn();
    }



    private boolean isFirstLaunch() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        return settings.getBoolean(ConstantStrings.SHOW_WELCOME, true);
    }


    private void showSetupScreen() {
        Intent i = new Intent(this, SetupActivity.class);
        startActivity(i);
        finish();
    }

}
