package com.apollodiary.ui.setup;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.apollodiary.ApolloDiaryApplication;
import com.apollodiary.EvernoteActivity;
import com.apollodiary.R;
import com.apollodiary.event.NotebookCreateError;
import com.apollodiary.event.NotebookCreatedEvent;
import com.apollodiary.event.NotebookFoundEvent;
import com.apollodiary.job.CreateNotebookJob;
import com.apollodiary.job.FetchNotebooksJob;
import com.apollodiary.ui.home.HomeScreenActivity;
import com.apollodiary.utils.ConstantStrings;
import com.evernote.client.android.EvernoteSession;
import com.path.android.jobqueue.JobManager;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SetupActivity extends EvernoteActivity {

    JobManager jobManager;
    EvernoteSession evernoteSession;


    @Override
    protected void setEventBusRegister() {
        isNeedRegister = true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!showSetup()) {
            showHomeScreen();
        }

        setContentView(R.layout.setup_screen);
        ButterKnife.inject(this);
        setEventBusRegister();

        jobManager = ApolloDiaryApplication.getJobManager();
        evernoteSession = ApolloDiaryApplication.getEvernoteSession();
    }

    private boolean showSetup() {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        return settings.getBoolean(ConstantStrings.SHOW_SETUP, true);
    }


    @SuppressWarnings("unused")
    @OnClick(R.id.create_notebook_button)
    public void createNotebook() {
        jobManager.addJobInBackground(new CreateNotebookJob(evernoteSession, ConstantStrings.APOLLO_DIARY));
        Toast.makeText(this, "Creating Notebook", Toast.LENGTH_SHORT).show();
    }



    @SuppressWarnings("unused")
    @OnClick(R.id.sync_account_button)
    public void syncAccountButton() {
        jobManager.addJobInBackground(new FetchNotebooksJob(evernoteSession));
        Toast.makeText(this, "Syncing Account", Toast.LENGTH_SHORT).show();
    }


    private void showHomeScreen(){
        Intent i;
        i = new Intent(this, HomeScreenActivity.class);
        startActivity(i);
        finish();
    }


    public void onEventMainThread(NotebookCreateError notebookCreateError) {
        if(notebookCreateError.getReason().equalsIgnoreCase("EXISTING_NOTEBOOK")) {
            Toast.makeText(this, "Notebook Already Exists!", Toast.LENGTH_SHORT).show();
            jobManager.addJobInBackground(new FetchNotebooksJob(evernoteSession));
        }
    }


    public void onEventMainThread(NotebookCreatedEvent notebookCreatedEvent) {
        Toast.makeText(this, "Notebook Created", Toast.LENGTH_SHORT).show();
        showHomeScreen();
    }

    public void onEventMainThread(NotebookFoundEvent notebookFoundEvent) {
        Toast.makeText(this, "Account Synced", Toast.LENGTH_SHORT).show();
        showHomeScreen();
    }

}
