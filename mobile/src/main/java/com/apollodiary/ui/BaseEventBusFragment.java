package com.apollodiary.ui;

import android.app.Fragment;
import android.os.Bundle;

import com.apollodiary.ApolloDiaryApplication;

import de.greenrobot.event.EventBus;

public class BaseEventBusFragment extends Fragment{

    protected EventBus eventBus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eventBus = ApolloDiaryApplication.getEventBus();
    }



    @Override
    public void onResume() {
        super.onResume();
        eventBus.register(this);
    }


    @Override
    public void onPause() {
        super.onPause();
        eventBus.unregister(this);
    }




}
