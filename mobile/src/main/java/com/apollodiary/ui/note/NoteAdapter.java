package com.apollodiary.ui.note;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.apollodiary.R;
import com.apollodiary.utils.DateUtils;
import com.evernote.edam.notestore.NoteMetadata;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

import static com.apollodiary.utils.DateUtils.DateFormat.TIME_FORMAT_24HR;

public class NoteAdapter extends ArrayAdapter<NoteMetadata> {

    private int resource;

    public NoteAdapter(Context context, int resource, List<NoteMetadata> notes) {
        super(context, resource, notes);
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;
        NoteMetadata noteMetadata = getItem(position);

        if (convertView != null) {
            viewHolder = (ViewHolder) convertView.getTag();
        } else {
            convertView = LayoutInflater.from(getContext()).inflate(resource, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }

        viewHolder.noteTitle.setText(noteMetadata.getTitle());

        long created = noteMetadata.getCreated();

        setNoteCreatedText(created,
                viewHolder.noteCreatedDay,
                viewHolder.noteCreatedDate,
                viewHolder.noteCreatedMonth,
                viewHolder.noteCreatedYear,
                viewHolder.noteCreatedTime);


        return convertView;
    }

    private void setNoteCreatedText(long created, TextView day, TextView date, TextView month, TextView year, TextView time) {

        String createdDayString = DateUtils.getDayOfWeekShort(created);
        day.setText(createdDayString);

        String createdDateString = DateUtils.getDayOfMonth(created);
        date.setText(createdDateString);

        String createdMonthString = DateUtils.getMonthOfYearShort(created);
        month.setText(createdMonthString);

        String createdYearString = DateUtils.getYearLong(created);
        year.setText(createdYearString);

        String createdTimeString = DateUtils.getDateTimeStringFromMillis(created, TIME_FORMAT_24HR.formatPattern);
        time.setText(createdTimeString);
    }


    static class ViewHolder {
        @InjectView(R.id.home_listview_item_row_note_title) TextView noteTitle;
        @InjectView(R.id.home_listview_item_row_note_created_day) TextView noteCreatedDay;
        @InjectView(R.id.home_listview_item_row_note_created_date) TextView noteCreatedDate;
        @InjectView(R.id.home_listview_item_row_note_created_month) TextView noteCreatedMonth;
        @InjectView(R.id.home_listview_item_row_note_created_year) TextView noteCreatedYear;
        @InjectView(R.id.home_listview_item_row_note_created_time) TextView noteCreatedTime;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }

    }
}
