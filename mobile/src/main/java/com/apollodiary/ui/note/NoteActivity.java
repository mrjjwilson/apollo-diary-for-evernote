package com.apollodiary.ui.note;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.apollodiary.EvernoteActivity;
import com.apollodiary.R;
import com.apollodiary.event.ImageChosenEvent;
import com.apollodiary.utils.ConstantStrings;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ChosenVideo;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.kbeanie.imagechooser.api.VideoChooserListener;
import com.kbeanie.imagechooser.api.VideoChooserManager;

import de.greenrobot.event.EventBus;


public class NoteActivity extends EvernoteActivity implements ImageChooserListener, VideoChooserListener {

    private static final String LOGTAG = NoteActivity.class.getSimpleName();

    private ImageChooserManager imageChooserManager;
    private VideoChooserManager videoChooserManager;
    private String filePath;

    @Override
    protected void setEventBusRegister() {
        isNeedRegister = false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_note);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        NoteType noteType = (NoteType) getIntent().getSerializableExtra(ConstantStrings.NOTE_TYPE);
        String noteGuid = getIntent().getStringExtra(ConstantStrings.NOTE_GUID);

        if (savedInstanceState == null) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            NoteFragment noteFragment = NoteFragment.newInstance(noteType, noteGuid);
            fragmentTransaction.add(R.id.create_note_container, noteFragment);
            fragmentTransaction.commit();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            switch (requestCode) {
                case ChooserType.REQUEST_PICK_PICTURE:
                case ChooserType.REQUEST_CAPTURE_PICTURE:
                    imageChooserManager.submit(requestCode, data);
                    break;

                case ChooserType.REQUEST_PICK_VIDEO:
                case ChooserType.REQUEST_CAPTURE_VIDEO:
                    videoChooserManager.submit(requestCode, data);
                    break;

                default:
                    throw new UnsupportedOperationException("Only Picture or Video selection supported");

            }

        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.ACTION_UP) {
            //TODO: JERRY - Check if new text has been added
            promptExitConfirmation();
            return true;
        }


        return super.onKeyDown(keyCode, event);
    }

    private void promptExitConfirmation() {


        AlertDialog alertDialog = new AlertDialog.Builder(this)
                //TODO: JERRY - Move to string resources so that this can be styled
                .setTitle(getString(R.string.exitConfirmationTitle))
                .setMessage(getString(R.string.exitConfirmationMessage))
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();

        TextView titleView = (TextView)alertDialog.findViewById(
                this.getResources().getIdentifier("alertTitle", "id", "android"));
        titleView.setTextColor(Color.BLACK);
        titleView.setGravity(Gravity.CENTER_HORIZONTAL);

        TextView messageView = (TextView)alertDialog.findViewById(android.R.id.message);
        messageView.setGravity(Gravity.CENTER_HORIZONTAL);


    }

    public void takePicture() {
        imageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_CAPTURE_PICTURE);
        imageChooserManager.setImageChooserListener(this);
        try {
            filePath = imageChooserManager.choose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void chooseImage() {
        imageChooserManager = new ImageChooserManager(this, ChooserType.REQUEST_PICK_PICTURE);
        imageChooserManager.setImageChooserListener(this);
        try {
            filePath = imageChooserManager.choose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void recordVideo() {
        videoChooserManager = new VideoChooserManager(this, ChooserType.REQUEST_CAPTURE_VIDEO);
        videoChooserManager.setVideoChooserListener(this);
        try {
            filePath = videoChooserManager.choose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void chooseVideo() {
        videoChooserManager = new VideoChooserManager(this, ChooserType.REQUEST_PICK_VIDEO);
        videoChooserManager.setVideoChooserListener(this);
        try {
            filePath = videoChooserManager.choose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onImageChosen(final ChosenImage chosenImage) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (chosenImage != null) {
                    //TODO: JERRY - Do something with image + fire event
                    EventBus.getDefault().post(new ImageChosenEvent(chosenImage));
//                    chosenImage.getFilePathOriginal();
                    // chosenImage.getFileThumbnail();
                    // chosenImage.getFileThumbnailSmall();
                }
            }
        });

    }

    @Override
    public void onVideoChosen(final ChosenVideo chosenVideo) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (chosenVideo != null) {
                    //TODO: JERRY - Do something with video + fire event
                    // video.getFilePathOriginal();
                    // video.getFileThumbnail();
                    // video.getFileThumbnailSmall();
                }
            }
        });
    }

    @Override
    public void onError(final String reason) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //TODO: JERRY - Show error message
            }
        });

    }
}
