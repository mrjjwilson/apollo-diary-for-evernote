package com.apollodiary.ui.note;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.apollodiary.ApolloDiaryApplication;
import com.apollodiary.R;
import com.apollodiary.event.ConnectionErrorEvent;
import com.apollodiary.event.DeletedNoteEvent;
import com.apollodiary.event.FetchedNoteEvent;
import com.apollodiary.event.InvalidNoteEvent;
import com.apollodiary.event.MissingNotebookEvent;
import com.apollodiary.event.NoteCreateErrorEvent;
import com.apollodiary.event.NoteCreatedEvent;
import com.apollodiary.event.NoteStoreErrorEvent;
import com.apollodiary.event.NoteSyncingEvent;
import com.apollodiary.event.NoteUpdatedEvent;
import com.apollodiary.job.DeleteNoteJob;
import com.apollodiary.job.FetchNoteJob;
import com.apollodiary.job.SyncNoteJob;
import com.apollodiary.ui.BaseEventBusFragment;
import com.apollodiary.utils.ConstantStrings;
import com.apollodiary.utils.DateUtils;
import com.evernote.client.android.EvernoteSession;
import com.evernote.client.android.EvernoteUtil;
import com.evernote.edam.type.Note;
import com.path.android.jobqueue.JobManager;

import org.joda.time.DateTime;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;
import de.greenrobot.event.EventBus;

import static com.apollodiary.utils.DateUtils.DateFormat.TIME_FORMAT_24HR;

public class NoteFragment extends BaseEventBusFragment {

    @InjectView(R.id.create_note_date)
    TextView noteDate;

    @InjectView(R.id.create_note_time)
    TextView noteTime;

    @InjectView(R.id.view_note_title)
    TextView viewNoteTitle;

    @InjectView(R.id.edit_note_title)
    EditText editNoteTitle;

    @InjectView(R.id.view_note_content)
    TextView viewNoteContent;

    @InjectView(R.id.edit_note_content)
    EditText editNoteContent;

    @Optional
    @InjectView(R.id.note_progress_bar)
    ProgressBar noteProgressBar;

    //TODO: JERRY - Add these views back once File attachment is continued
//    @InjectView(R.id.imageThumbnail)
//    ImageView imageThumbnail;

//    @InjectView(R.id.file_attachment_view_divider)
//    View fileAttachmentViewDivider;

    private JobManager jobManager;
    private SyncNoteJob.Factory syncNoteJobFactory;
    private EvernoteSession evernoteSession;
    private NoteType noteType;
    private String noteGuid;
    private ActionBar supportActionBar;

    // Mandatory empty constructor for Fragment Manager to handle screen orientation changes
    public NoteFragment() {
    }

    public static NoteFragment newInstance(NoteType noteType, String noteGuid) {
        NoteFragment fragment = new NoteFragment();
        Bundle args = new Bundle();
        args.putSerializable(ConstantStrings.NOTE_TYPE, noteType);
        args.putString(ConstantStrings.NOTE_GUID, noteGuid);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        jobManager = ApolloDiaryApplication.getJobManager();
        syncNoteJobFactory = new SyncNoteJob.Factory();
        evernoteSession = ApolloDiaryApplication.getEvernoteSession();

        View rootView = inflater.inflate(R.layout.fragment_note, container, false);
        ButterKnife.inject(this, rootView);

        ActionBarActivity actionBarActivity = (ActionBarActivity) getActivity();
        supportActionBar = actionBarActivity.getSupportActionBar();

        noteType = (NoteType) getArguments().getSerializable(ConstantStrings.NOTE_TYPE);
        noteGuid = getArguments().getString(ConstantStrings.NOTE_GUID);

        setActionBarTitle(noteType);

        setDateTime(noteDate, noteTime);

        setHasOptionsMenu(true);

        if(noteType == NoteType.EXISTING_NOTE) {
            fetchExistingNote();
        }

        return rootView;
    }

    private void setDateTime(TextView noteDate, TextView noteTime) {
        String day = DateUtils.getDayOfWeekShort();
        String date = DateUtils.getDayOfMonth();
        String month = DateUtils.getMonthOfYearShort();
        String year = DateUtils.getYearShort();
        String whitespace = " ";

        noteDate.setText(day + whitespace + date + whitespace + month + whitespace + year);
        noteTime.setText(DateUtils.getDateTimeString(TIME_FORMAT_24HR.formatPattern));
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void fetchExistingNote() {
        editNoteTitle.setVisibility(View.GONE);
        viewNoteTitle.setVisibility(View.GONE);
        viewNoteContent.setVisibility(View.GONE);
        editNoteContent.setVisibility(View.GONE);
        noteProgressBar.setVisibility(View.VISIBLE);
        jobManager.addJobInBackground(new FetchNoteJob(noteGuid));
    }

    private void displayExistingNote(final String title, final String content) {
        noteProgressBar.setVisibility(View.GONE);
        viewNoteTitle.setVisibility(View.VISIBLE);
        viewNoteContent.setVisibility(View.VISIBLE);
        viewNoteTitle.setText(title);
        viewNoteContent.setText(content);
    }

    private void titleEditMode() {
        switchToEditMode(viewNoteTitle, editNoteTitle, viewNoteTitle.getText().toString());
    }

    private void contentEditMode() {
        switchToEditMode(viewNoteContent, editNoteContent, viewNoteContent.getText().toString());
    }

    private void switchToEditMode(TextView textView, EditText editText, String text) {
        editText.setVisibility(View.VISIBLE);
        editText.setText(text);
        textView.setVisibility(View.GONE);
    }


    private void setActionBarTitle(NoteType noteType) {
        switch (noteType) {
            case CREATE_NOTE:
                supportActionBar.setTitle(R.string.create_note_actionbar);
                break;
            case EXISTING_NOTE:
                supportActionBar.setTitle(R.string.view_note_actionbar);
                break;
            default:
                throw new IllegalArgumentException("Undefined NoteType" + noteType);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.menu_item_note_upload:
                syncNote();
                break;

            //TODO: JERRY - Add these back when File attachment returns
            case R.id.take_picture:
                takePicture();
                break;

            case R.id.choose_image:
                chooseImage();
                break;

            case R.id.record_video:
                recordVideo();
                break;

            case R.id.choose_video:
                chooseVideo();
                break;

            case R.id.menu_item_delete_note:
                showDeleteNotePopUp();
                break;

            case R.id.menu_item_edit_note:
                supportActionBar.setTitle(R.string.edit_note_actionbar);
                titleEditMode();
                contentEditMode();

            default:
                break;
        }

        return true;
    }

    private boolean isInstanceOfNoteActivity() {
        return getActivity() instanceof NoteActivity;
    }


    /**
     * File Attachment Handling Methods
     */
    private void chooseVideo() {
        if (isInstanceOfNoteActivity()) {
            ((NoteActivity) getActivity()).chooseVideo();
        }
    }


    private void recordVideo() {
        if (isInstanceOfNoteActivity()) {
            ((NoteActivity) getActivity()).recordVideo();
        }
    }


    private void chooseImage() {
        if (isInstanceOfNoteActivity()) {
            ((NoteActivity) getActivity()).chooseImage();
        }
    }

    private void takePicture() {
        if (isInstanceOfNoteActivity()) {
            ((NoteActivity) getActivity()).takePicture();
        }
    }

    /**
     * Note Modification
     */

    private void syncNote() {

        String title = editNoteTitle.getText().toString();
        String content = editNoteContent.getText().toString();

        if (isNoteEmpty(title, content)) {
            displayMessage(MessageType.EMPTY_CONTENT);
        } else {
            Note note = new Note();
            DateTime dt = new DateTime();
            note.setCreated(dt.getMillis());
            note.setTitle(title);
            note.setContent(EvernoteUtil.NOTE_PREFIX + content + EvernoteUtil.NOTE_SUFFIX);

            if(noteType == NoteType.EXISTING_NOTE) {
                note.setGuid(noteGuid);
                jobManager.addJobInBackground(syncNoteJobFactory.create(note));
            } else {
                jobManager.addJobInBackground(syncNoteJobFactory.create(note));
            }
        }

    }


    private void deleteNote() {
        jobManager.addJobInBackground(new DeleteNoteJob(noteGuid));
    }

    private void showDeleteNotePopUp() {

        String noteTitleString = viewNoteTitle.getText().toString();

        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                //TODO: JERRY - Move to string resources so that this can be styled
                .setTitle(getString(R.string.deleteNoteConfirmationTitle) + " " + noteTitleString)
                .setMessage(getString(R.string.deleteNoteConfirmationMessage))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteNote();
                        showDeleteNoteProgress();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();

        TextView titleView = (TextView)alertDialog.findViewById(
                this.getResources().getIdentifier("alertTitle", "id", "android"));
        titleView.setTextColor(Color.BLACK);
        titleView.setGravity(Gravity.CENTER_HORIZONTAL);

        TextView messageView = (TextView)alertDialog.findViewById(android.R.id.message);
        messageView.setGravity(Gravity.CENTER_HORIZONTAL);
    }

    private void showDeleteNoteProgress() {
        editNoteTitle.setVisibility(View.GONE);
        editNoteContent.setVisibility(View.GONE);
        noteProgressBar.setVisibility(View.VISIBLE);
    }

    private boolean isNoteEmpty(String title, String content) {
        return title.isEmpty() || content.isEmpty();
    }


    private void displayMessage(MessageType messageType) {

        switch (messageType) {

            case EMPTY_CONTENT:
                displayShortToast(R.string.empty_content_error);
                break;

            case NOTE_SAVING_FAIL:
                displayShortToast(R.string.error_saving_note);
                break;

            case NOTE_SAVING_SUCCESS:
                displayShortToast(R.string.note_saved);
                break;

            case NOTE_UPDATE_SUCCESS:
                displayShortToast(R.string.note_updated);
                break;

            case NOTE_STORE_ERROR:
                displayShortToast(R.string.error_creating_notestore);
                break;

            case NOTE_SYNCING:
                displayShortToast(R.string.syncing_with_evernote);
                break;

            case MISSING_NOTEBOOK:
                displayShortToast(R.string.missing_notebook);

            case CONNECTION_ERROR:
                displayShortToast(R.string.connection_error);

            default:
                break;
        }

    }

    private void displayShortToast(int message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }

    private void showView(View view) {
        if(view.getVisibility() == View.GONE || view.getVisibility() == View.INVISIBLE) {
            view.setVisibility(View.VISIBLE);
            }
    }

    /**
     * EventBus Handling methods
     */

//    //TODO: JERRY - Add the ImageChosenEvent back when File attachments are reintroduced
//    public void onEventMainThread(ImageChosenEvent imageChosenEvent) {
//        showView(fileAttachmentViewDivider);
//
//
//        //TODO: Decrement the counter by 1 if an image is removed
//
//        showView(imageThumbnail);
//        //TODO: JERRY - How will you handle multiple images and videos?
//        //TODO: JERRY - Create a new image view or video for each file
//        ChosenImage chosenImage = imageChosenEvent.getChosenImage();
//        imageThumbnail.setImageURI(Uri.parse(new File(chosenImage.getFileThumbnail()).toString()));
//
//        //TODO: Create a new image view each time this method is called, increment a counter by 1
//        ImageView imageView = new ImageView(getActivity());
//        imageView.setImageURI(Uri.parse(new File(chosenImage.getFileThumbnail()).toString()));
//        //TODO: JERRY - Attach this image to the note
//    }

    public void onEventMainThread(FetchedNoteEvent fetchedNoteEvent) {

        Note note = fetchedNoteEvent.getNote();
        String noteTitle = note.getTitle();
        String noteContent = fetchedNoteEvent.getNoteContent();

        displayExistingNote(noteTitle, noteContent);
        EventBus.getDefault().removeStickyEvent(FetchedNoteEvent.class);
    }

    public void onEventMainThread(DeletedNoteEvent deletedNoteEvent) {
        getActivity().finish();
    }

    public void onEventMainThread(NoteSyncingEvent noteSyncingEvent) {
        displayMessage(MessageType.NOTE_SYNCING);
    }

    public void onEventMainThread(NoteCreatedEvent noteCreatedEvent) {
        displayMessage(MessageType.NOTE_SAVING_SUCCESS);
    }

    public void onEventMainThread(NoteUpdatedEvent noteUpdatedEvent) {
        displayMessage(MessageType.NOTE_UPDATE_SUCCESS);
    }

    public void onEventMainThread(NoteCreateErrorEvent noteCreateErrorEvent) {
        displayMessage(MessageType.NOTE_SAVING_FAIL);
    }

    public void onEventMainThread(InvalidNoteEvent invalidNoteEvent) {
        displayMessage(MessageType.EMPTY_CONTENT);
    }

    public void onEventMainThread(NoteStoreErrorEvent noteStoreErrorEvent) {
        displayMessage(MessageType.NOTE_STORE_ERROR);
    }

    public void onEventMainThread(MissingNotebookEvent missingNotebookEvent) {
        displayMessage(MessageType.MISSING_NOTEBOOK);
    }

    public void onEventMainThread(ConnectionErrorEvent connectionErrorEvent) {
        displayMessage(MessageType.CONNECTION_ERROR);
    }

    public enum MessageType {
        EMPTY_CONTENT,
        NOTE_SAVING_FAIL,
        NOTE_SAVING_SUCCESS,
        NOTE_STORE_ERROR,
        NOTE_SYNCING,
        MISSING_NOTEBOOK,
        CONNECTION_ERROR,
        NOTE_UPDATE_SUCCESS;
    }

}
