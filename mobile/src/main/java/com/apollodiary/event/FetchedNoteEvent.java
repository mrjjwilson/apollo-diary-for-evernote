package com.apollodiary.event;

import com.evernote.edam.type.Note;

public class FetchedNoteEvent {

    private Note note;
    private final String noteContent;

    public FetchedNoteEvent(Note note, String noteContent) {
        this.note = note;
        this.noteContent = noteContent;
    }

    public Note getNote() {
        return note;
    }

    public String getNoteContent() {
        return noteContent;
    }
}
