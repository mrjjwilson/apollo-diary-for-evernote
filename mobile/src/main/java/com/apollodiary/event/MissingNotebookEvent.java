package com.apollodiary.event;

public class MissingNotebookEvent {
    private String notebookName;

    public MissingNotebookEvent(String notebookName) {
        this.notebookName = notebookName;
    }

    public MissingNotebookEvent() {
    }

    public String getNotebookName() {
        return notebookName;
    }
}
