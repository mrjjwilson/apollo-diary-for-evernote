package com.apollodiary.event;

import com.kbeanie.imagechooser.api.ChosenImage;

public class ImageChosenEvent {

    ChosenImage chosenImage;

    public ImageChosenEvent(ChosenImage chosenImage) {
        this.chosenImage = chosenImage;
    }

    public ChosenImage getChosenImage() {
        return chosenImage;
    }
}
