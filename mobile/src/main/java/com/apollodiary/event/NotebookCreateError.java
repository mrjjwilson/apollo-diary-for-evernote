package com.apollodiary.event;

public class NotebookCreateError {

    private String dataConflict;
    private String reason;

    public NotebookCreateError(String dataConflict, String reason) {
        this.dataConflict = dataConflict;
        this.reason = reason;
    }

    public String getReason() {
        return reason;
    }

    public String getDataConflict() {
        return dataConflict;
    }
}
