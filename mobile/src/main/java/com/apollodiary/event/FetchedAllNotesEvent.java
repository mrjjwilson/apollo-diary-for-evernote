package com.apollodiary.event;

import com.evernote.edam.notestore.NoteMetadata;
import com.evernote.edam.notestore.NotesMetadataList;

import java.util.List;

public class FetchedAllNotesEvent {

    private NotesMetadataList notesMetadataList;

    public FetchedAllNotesEvent(NotesMetadataList notesMetadataList) {
        this.notesMetadataList = notesMetadataList;
    }

    public List<NoteMetadata> getAllNotes() {
        return notesMetadataList.getNotes();
    }

}
