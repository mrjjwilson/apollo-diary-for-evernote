package com.apollodiary.event;

public class NotebookErrorEvent {
    private final String error;

    public NotebookErrorEvent(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public boolean isNotebookMissing() {
        return error.contains("EDAMNotFoundException") && error.contains("Notebook");
    }
}
