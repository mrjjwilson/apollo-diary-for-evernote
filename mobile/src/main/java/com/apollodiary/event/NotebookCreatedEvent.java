package com.apollodiary.event;

import com.evernote.edam.type.Notebook;

public class NotebookCreatedEvent {
    private final Notebook notebook;

    public NotebookCreatedEvent(Notebook notebook) {
        this.notebook = notebook;
    }

    public String getNotebookGuid() {
        return notebook.getGuid();
    }

    public String getNotebookName() {
        return notebook.getName();
    }
}
