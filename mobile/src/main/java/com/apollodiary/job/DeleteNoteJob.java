package com.apollodiary.job;

import android.util.Log;

import com.apollodiary.ApolloDiaryApplication;
import com.apollodiary.event.DeletedNoteEvent;
import com.apollodiary.event.NoteStoreErrorEvent;
import com.apollodiary.helper.NetworkHelper;
import com.evernote.client.android.EvernoteSession;
import com.evernote.client.android.OnClientCallback;
import com.evernote.thrift.transport.TTransportException;
import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import de.greenrobot.event.EventBus;

public class DeleteNoteJob extends Job {

    public static final int PRIORITY = 1;
    private static final String LOGTAG = FetchNoteJob.class.getSimpleName();
    private final EvernoteSession evernoteSession;
    private final NetworkHelper networkHelper;
    private final String noteGuid;

    public DeleteNoteJob(String noteGuid) {
        // This job requires network connection, so persist in case app exists before completed
        super(new Params(PRIORITY).requireNetwork().persist());
        this.noteGuid = noteGuid;
        evernoteSession = ApolloDiaryApplication.getEvernoteSession();
        networkHelper = new NetworkHelper();
    }


    @Override
    public void onAdded() {
        if(networkHelper.isNetworkOnline()) {
            deleteNote(noteGuid);
        } else {

        }

    }

    @Override
    public void onRun() throws Throwable {

    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        return false;
    }

    private void deleteNote(String noteGuid) {

        try {
            evernoteSession
                    .getClientFactory()
                    .createNoteStoreClient()
                    .deleteNote(noteGuid, getOnClientCallback());

        } catch (TTransportException exception) {
            Log.e(LOGTAG, "Error creating notestore", exception);
            EventBus.getDefault().post(new NoteStoreErrorEvent());
        }

    }

    private OnClientCallback<Integer> getOnClientCallback() {
        return new OnClientCallback<Integer>() {

            @Override
            public void onSuccess(Integer data) {
                EventBus.getDefault().postSticky(new DeletedNoteEvent());
            }

            @Override
            public void onException(Exception exception) {
                Log.e("Delete Note Error", exception.getCause().toString());
            }
        };
    }


}
