package com.apollodiary.job;

import android.util.Log;

import com.apollodiary.ApolloDiaryApplication;
import com.apollodiary.event.InvalidNoteEvent;
import com.apollodiary.event.MissingNotebookEvent;
import com.apollodiary.event.NoteCreateErrorEvent;
import com.apollodiary.event.NoteCreatedEvent;
import com.apollodiary.event.NoteStoreErrorEvent;
import com.apollodiary.helper.NoteHelper;
import com.apollodiary.helper.NotebookHelper;
import com.evernote.client.android.EvernoteSession;
import com.evernote.client.android.OnClientCallback;
import com.evernote.edam.type.Note;
import com.evernote.thrift.transport.TTransportException;
import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import de.greenrobot.event.EventBus;

public class CreateNoteJob extends Job {

    public static final int PRIORITY = 1;
    private static final String LOGTAG = CreateNoteJob.class.getSimpleName();

    private Note note;

    public CreateNoteJob(Note note) {
        // This job requires network connection, so persist in case app exists before completed
        super(new Params(PRIORITY).requireNetwork().persist());
        this.note = note;
    }

    @Override
    public void onAdded() {
    }

    @Override
    public void onRun() throws Throwable {
        createNote(note);
    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        return false;
    }

    private void createNote(Note note) {

        if (NoteHelper.isNoteValid(note)) {

            if (NotebookHelper.isDefaultNotebookSet()) {
                note.setNotebookGuid(NotebookHelper.getDefaultNotebookGuid());
                createNoteInEvernoteSession(ApolloDiaryApplication.getEvernoteSession(), note);

            } else {
                // A default notebook has not been set -- The app should only have 1 linked notebook
                //TODO: JERRY - Give user the option to set a default notebook
                EventBus.getDefault().post(new MissingNotebookEvent());
            }

        } else {
            //TODO: JERRY - Pass data about what part of the Note was Invalid
            EventBus.getDefault().post(new InvalidNoteEvent());
        }
    }

    private void createNoteInEvernoteSession(EvernoteSession evernoteSession, Note note) {
        try {
            evernoteSession.getClientFactory().createNoteStoreClient().createNote(note, getNoteCreateClientCallBack());
        } catch (TTransportException exception) {
            Log.e(LOGTAG, "Error creating notestore", exception);
            EventBus.getDefault().post(new NoteStoreErrorEvent());
        }
    }

    private static OnClientCallback<Note> getNoteCreateClientCallBack() {

        return new OnClientCallback<Note>() {
            @Override
            public void onSuccess(Note note) {
                //TODO: JERRY - This is only received if the adapter is separated from the UI
                //TODO: JERRY - Implement a Custom Adapter to get this working
                EventBus.getDefault().postSticky(new NoteCreatedEvent());
            }

            @Override
            public void onException(Exception exception) {
                Log.e(LOGTAG, "Error saving note", exception);
                EventBus.getDefault().post(new NoteCreateErrorEvent());
            }
        };
    }


}