package com.apollodiary.job;

import com.apollodiary.event.ConnectionErrorEvent;
import com.apollodiary.helper.NetworkHelper;
import com.evernote.edam.type.Note;
import com.apollodiary.event.NoteSyncingEvent;
import com.apollodiary.ApolloDiaryApplication;
import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.JobManager;
import com.path.android.jobqueue.Params;

import de.greenrobot.event.EventBus;

public class SyncNoteJob extends Job {

    public static final int PRIORITY = 1;

    private Note note;
    private JobManager jobManager;
    private NetworkHelper networkHelper;

    protected SyncNoteJob(Note note, JobManager jobManager) {
        super(new Params(PRIORITY).requireNetwork().persist());
        this.note = note;
        this.jobManager = jobManager;
        networkHelper = new NetworkHelper();
    }

    @Override
    public void onAdded() {

        if(networkHelper.isNetworkOnline()) {
            //This logic is implemented here so that the decision to Append or Create a new Note is made straight away
            EventBus.getDefault().post(new NoteSyncingEvent());

            if (note.isSetGuid()) {
                jobManager.addJobInBackground(new AppendNoteJob(note));
            } else {
                jobManager.addJobInBackground(new CreateNoteJob(note));
            }
        } else {
            ApolloDiaryApplication.getEventBus().post(new ConnectionErrorEvent());
        }
    }

    @Override
    public void onRun() throws Throwable {
    }

    @Override
    protected void onCancel() {
    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        return false;
    }

    public static class Factory {

        private JobManager jobManager;

        public Factory() {
            this.jobManager = ApolloDiaryApplication.getJobManager();
        }

        public SyncNoteJob create(Note note) {
            return new SyncNoteJob(note, jobManager);
        }


    }
}
