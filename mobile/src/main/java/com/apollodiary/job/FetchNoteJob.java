package com.apollodiary.job;

import android.util.Log;

import com.apollodiary.ApolloDiaryApplication;
import com.apollodiary.event.ConnectionErrorEvent;
import com.apollodiary.event.FetchedNoteEvent;
import com.apollodiary.event.NoteStoreErrorEvent;
import com.apollodiary.helper.NetworkHelper;
import com.evernote.client.android.EvernoteSession;
import com.evernote.client.android.OnClientCallback;
import com.evernote.edam.type.Note;
import com.evernote.thrift.transport.TTransportException;
import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.greenrobot.event.EventBus;

public class FetchNoteJob extends Job{

    public static final int PRIORITY = 1;
    private static final String LOGTAG = FetchNoteJob.class.getSimpleName();
    private final EvernoteSession evernoteSession;
    private final String noteGuid;
    private final NetworkHelper networkHelper;

    public FetchNoteJob(String noteGuid) {
        // This job requires network connection, so persist in case app exists before completed
        super(new Params(PRIORITY).requireNetwork().persist());
        evernoteSession = ApolloDiaryApplication.getEvernoteSession();
        this.noteGuid = noteGuid;
        networkHelper = new NetworkHelper();
    }

    @Override
    public void onAdded() {
        if (networkHelper.isNetworkOnline()) {
            fetchNote(evernoteSession);
        } else {
            ApolloDiaryApplication.getEventBus().post(new ConnectionErrorEvent());
        }

    }

    @Override
    public void onRun() throws Throwable {

    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        return false;
    }

    private void fetchNote(EvernoteSession evernoteSession) {

        try {
            evernoteSession
                    .getClientFactory()
                    .createNoteStoreClient()
                    .getNote(noteGuid, true, false, false, false, getOnClientCallback());

        } catch (TTransportException exception) {
            Log.e(LOGTAG, "Error creating notestore", exception);
            EventBus.getDefault().post(new NoteStoreErrorEvent());
        }

    }


    private OnClientCallback<Note> getOnClientCallback() {
        return new OnClientCallback<Note>() {

            @Override
            public void onSuccess(Note note) {
                String noteContent = extractNoteContent(note);
                EventBus.getDefault().postSticky(new FetchedNoteEvent(note, noteContent));
            }

            @Override
            public void onException(Exception exception) {
                Log.e(LOGTAG, exception.getCause().toString());
            }
        };
    }

    private String extractNoteContent(Note note) {

        String noteRegex = "<en-note>(.*)</en-note>";
        String contentString = note.getContent();

        Pattern pattern = Pattern.compile(noteRegex, Pattern.DOTALL);
        Matcher matcher = pattern.matcher(contentString);

        String noteContent = "";

        if(matcher.find()) {
            noteContent = matcher.group(1);
        } else {
            Log.e(LOGTAG, "Could not find note content");
        }

        return noteContent;
    }


}
