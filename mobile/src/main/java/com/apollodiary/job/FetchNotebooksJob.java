package com.apollodiary.job;

import android.util.Log;

import com.apollodiary.ApolloDiaryApplication;
import com.apollodiary.event.NotebookFoundEvent;
import com.apollodiary.helper.NotebookHelper;
import com.apollodiary.utils.ConstantStrings;
import com.evernote.client.android.EvernoteSession;
import com.evernote.client.android.OnClientCallback;
import com.evernote.edam.type.Notebook;
import com.evernote.thrift.transport.TTransportException;
import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.JobManager;
import com.path.android.jobqueue.Params;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

public class FetchNotebooksJob extends Job {

    private static final String LOGTAG = FetchNotebooksJob.class.getSimpleName();
    private EvernoteSession evernoteSession;
    private ApolloDiaryApplication application;

    public static final int PRIORITY = 1;


    public FetchNotebooksJob(EvernoteSession evernoteSession) {
        // This job requires network connection, so persist in case app exists before completed
        super(new Params(PRIORITY).requireNetwork().persist());
        this.evernoteSession = evernoteSession;
    }

    @Override
    public void onAdded() {
        Log.i(LOGTAG, "Fetching Notebook Job added");
        //TODO: JERRY - Check the network connectivity
        retrieveNotebooks(evernoteSession);
    }

    @Override
    public void onRun() throws Throwable {
//        retrieveNotebooks(evernoteSession, application);
    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        return false;
    }


    private void retrieveNotebooks(EvernoteSession evernoteSession) {
        try {
            evernoteSession.getClientFactory().createNoteStoreClient().listNotebooks(new OnClientCallback<List<Notebook>>() {
                @Override
                public void onSuccess(List<Notebook> notebookList) {
                    searchNotebookList(notebookList);
                }

                @Override
                public void onException(Exception exception) {
                    //TODO: JERRY - Fire event
                    Log.e(LOGTAG, exception.getCause().toString());
                }
            });
        } catch (TTransportException e) {
            e.printStackTrace();
        }
    }

    private void searchNotebookList(List<Notebook> notebookList) {

        List<Notebook> existingNotebook = new ArrayList<Notebook>();

        for(Notebook notebook : notebookList) {
            if(notebook.getName().equalsIgnoreCase(ConstantStrings.APOLLO_DIARY)) {
                existingNotebook.add(notebook);
            }
        }

        if(!existingNotebook.isEmpty()) {

            //TODO: JERRY - Looping through the notebookList will trigger both the IF when it matches and the ELSE on every other notebook - THIS IS WRONG
            // I ONLY want to check if the Apollo Diary exists AT ALL in a list of notebook names.

            Notebook apolloNotebook = existingNotebook.get(0);

                    if(NotebookHelper.getDefaultNotebookGuid() == null) {
                        NotebookHelper.setDefaultNotebookGuid(apolloNotebook.getGuid());
                    } else if(!NotebookHelper.getDefaultNotebookGuid().equalsIgnoreCase(apolloNotebook.getGuid())){
                        NotebookHelper.setDefaultNotebookGuid(apolloNotebook.getGuid());
                    }
                    if(NotebookHelper.getDefaultNotebookName() == null) {
                        NotebookHelper.setDefaultNotebookName(apolloNotebook.getName());
                    }
            EventBus.getDefault().post(new NotebookFoundEvent());

        } else {

            Log.e(LOGTAG, "Cannot find " + ConstantStrings.APOLLO_DIARY + " creating new notebook");
            JobManager jobManager = ApolloDiaryApplication.getJobManager();
            jobManager.addJobInBackground(new CreateNotebookJob(evernoteSession, ConstantStrings.APOLLO_DIARY));
        }
    }


}
