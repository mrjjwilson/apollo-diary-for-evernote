package com.apollodiary.job;

import android.util.Log;

import com.apollodiary.ApolloDiaryApplication;
import com.apollodiary.event.ConnectionErrorEvent;
import com.apollodiary.event.FetchedAllNotesEvent;
import com.apollodiary.event.FetchingAllNotesEvent;
import com.apollodiary.event.NoteStoreErrorEvent;
import com.apollodiary.event.NotebookErrorEvent;
import com.apollodiary.event.NotesNotFoundEvent;
import com.apollodiary.helper.NetworkHelper;
import com.evernote.client.android.EvernoteSession;
import com.evernote.client.android.OnClientCallback;
import com.evernote.edam.notestore.NoteFilter;
import com.evernote.edam.notestore.NotesMetadataList;
import com.evernote.edam.notestore.NotesMetadataResultSpec;
import com.evernote.edam.type.NoteSortOrder;
import com.evernote.thrift.transport.TTransportException;
import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import de.greenrobot.event.EventBus;

public class FetchAllNotesJob extends Job {

    public static final int PRIORITY = 1;
    private static final String LOGTAG = FetchAllNotesJob.class.getSimpleName();

    private EvernoteSession evernoteSession;
    private NetworkHelper networkHelper;
    private String notebookGuid;

    public FetchAllNotesJob(String notebookGuid) {
        // This job requires network connection, so persist in case app exists before completed
        super(new Params(PRIORITY).requireNetwork().persist());
        evernoteSession = ApolloDiaryApplication.getEvernoteSession();
        this.notebookGuid = notebookGuid;
        networkHelper = new NetworkHelper();
    }

    @Override
    public void onAdded() {

        if (networkHelper.isNetworkOnline()) {
            EventBus.getDefault().post(new FetchingAllNotesEvent());
            fetchAllNotes(evernoteSession);
        } else {
            ApolloDiaryApplication.getEventBus().post(new ConnectionErrorEvent());
        }
    }

    @Override
    public void onRun() throws Throwable {
    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        return false;
    }


    private void fetchAllNotes(EvernoteSession evernoteSession) {

        final int offset = 0;
        final int maxNotes = 100;

        NoteFilter filter = new NoteFilter();
        filter.setOrder(NoteSortOrder.UPDATED.getValue());
        filter.setNotebookGuid(notebookGuid);

        final NotesMetadataResultSpec resultSpec = new NotesMetadataResultSpec();
        resultSpec.setIncludeTitle(true);
        resultSpec.setIncludeCreated(true);
        resultSpec.setIncludeNotebookGuid(true);
        resultSpec.setIncludeDeleted(true);
        resultSpec.setIncludeAttributes(true);

        try {
            evernoteSession
                    .getClientFactory()
                    .createNoteStoreClient()
                    .findNotesMetadata(filter, offset, maxNotes, resultSpec, getOnClientCallback());

        } catch (TTransportException exception) {
            Log.e(LOGTAG, "Error creating notestore", exception);
            EventBus.getDefault().post(new NoteStoreErrorEvent());
        }
    }

    private OnClientCallback<NotesMetadataList> getOnClientCallback() {
        return new OnClientCallback<NotesMetadataList>() {
            @Override
            public void onSuccess(NotesMetadataList data) {
                if (data.getTotalNotes() == 0) {
                    EventBus.getDefault().post(new NotesNotFoundEvent());
                } else {
                    EventBus.getDefault().post(new FetchedAllNotesEvent(data));
                }

            }

            @Override
            public void onException(Exception exception) {

                String error = exception.getCause().toString();
                if(error.contains("EDAMNotFoundException") && error.contains("Notebook")) {
                    Log.e(LOGTAG, error);
                    EventBus.getDefault().post(new NotebookErrorEvent(error));
                }
            }
        };
    }


}
