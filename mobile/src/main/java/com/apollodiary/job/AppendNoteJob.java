package com.apollodiary.job;

import android.util.Log;

import com.apollodiary.ApolloDiaryApplication;
import com.apollodiary.event.ConnectionErrorEvent;
import com.apollodiary.event.NoteStoreErrorEvent;
import com.apollodiary.event.NoteUpdatedEvent;
import com.apollodiary.helper.NetworkHelper;
import com.evernote.client.android.EvernoteSession;
import com.evernote.client.android.OnClientCallback;
import com.evernote.edam.type.Note;
import com.evernote.thrift.transport.TTransportException;
import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import de.greenrobot.event.EventBus;

public class AppendNoteJob extends Job {

    public static final int PRIORITY = 1;
    private static final String LOGTAG = AppendNoteJob.class.getSimpleName();
    private final NetworkHelper networkHelper;

    private Note note;
    private EvernoteSession evernoteSession;

    protected AppendNoteJob(Note note) {
        super(new Params(PRIORITY).requireNetwork().persist());
        this.note = note;
        this.evernoteSession = ApolloDiaryApplication.getEvernoteSession();
        networkHelper = new NetworkHelper();
    }


    @Override
    public void onAdded() {

        if(networkHelper.isNetworkOnline()) {
            updateNote(evernoteSession, note);
        } else {
            ApolloDiaryApplication.getEventBus().post(new ConnectionErrorEvent());
        }

    }

    @Override
    public void onRun() throws Throwable {


    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        return false;
    }


    private void updateNote(EvernoteSession evernoteSession, Note note) {

        try {
            evernoteSession
                    .getClientFactory()
                    .createNoteStoreClient()
                    .updateNote(note, getOnClientCallback());

        } catch (TTransportException exception) {
            Log.e(LOGTAG, "Error creating notestore", exception);
            EventBus.getDefault().post(new NoteStoreErrorEvent());
        }
    }

    private OnClientCallback<Note> getOnClientCallback() {
        return new OnClientCallback<Note>() {
            @Override
            public void onSuccess(Note data) {
                EventBus.getDefault().postSticky(new NoteUpdatedEvent());
            }

            @Override
            public void onException(Exception exception) {
                Log.e(LOGTAG, exception.getCause().toString());
            }
        };
    }

}
