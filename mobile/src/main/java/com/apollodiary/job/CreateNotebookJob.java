package com.apollodiary.job;

import android.util.Log;

import com.apollodiary.event.NotebookCreateError;
import com.apollodiary.event.NotebookCreatedEvent;
import com.apollodiary.helper.NotebookHelper;
import com.evernote.client.android.EvernoteSession;
import com.evernote.client.android.OnClientCallback;
import com.evernote.edam.type.Notebook;
import com.evernote.thrift.transport.TTransportException;
import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;

import de.greenrobot.event.EventBus;

public class CreateNotebookJob extends Job {

    public static final int PRIORITY = 1;
    private static final String NOTEBOOK_NAME = "Notebook Name= ";
    private static final String NOTEBOOK_GUID = "Notebook Guid= ";
    private final String notebookName;
    private EvernoteSession evernoteSession;

    public CreateNotebookJob(EvernoteSession evernoteSession,
                             String notebookName) {
        super(new Params(PRIORITY).requireNetwork().persist());
        this.evernoteSession = evernoteSession;
        this.notebookName = notebookName;
    }

    @Override
    public void onAdded() {
        Notebook apolloNotebook = new Notebook();
        apolloNotebook.setName(notebookName);
        apolloNotebook.setDefaultNotebook(true);

        createNotebook(apolloNotebook, evernoteSession);

    }

    @Override
    public void onRun() throws Throwable {
    }

    @Override
    protected void onCancel() {

    }

    @Override
    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        return false;
    }

    private void createNotebook(Notebook notebook, EvernoteSession evernoteSession) {

        try {
            evernoteSession.getClientFactory().createNoteStoreClient().createNotebook(notebook, new OnClientCallback<Notebook>() {
                @Override
                public void onSuccess(Notebook notebook) {
                    EventBus.getDefault().post(new NotebookCreatedEvent(notebook));
                    NotebookHelper.setDefaultNotebookName(notebook.getName());
                    NotebookHelper.setDefaultNotebookGuid(notebook.getGuid());

                    Log.i("New Notebook Created!", NOTEBOOK_NAME
                                    + notebook.getName()
                                    + NOTEBOOK_GUID
                                    + notebook.getGuid()
                    );
                }

                @Override
                public void onException(Exception exception) {

                    String dataConflict = exception.getCause().toString();

                    if (dataConflict.contains("DATA_CONFLICT") && dataConflict.contains("Notebook.name")) {
                        String reason = "EXISTING_NOTEBOOK";
                        Log.e("EXISTING_NOTEBOOK", dataConflict);
                        EventBus.getDefault().post(new NotebookCreateError(dataConflict, reason));
                    }
                }
            });

        } catch (TTransportException e) {
            e.printStackTrace();

        }

    }

}
