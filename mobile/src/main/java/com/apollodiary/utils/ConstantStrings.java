package com.apollodiary.utils;


public final class ConstantStrings {

    //TODO: JERRY - Check which values can be Enums instead and put into specific classes (LogUtils etc) ConstantStrings is too generic

    public static final String SHOW_WELCOME = "show_welcome_screen";
    public static final String APOLLO_DIARY = "Apollo Diary";

    public static final String SHOW_SETUP = "show_setup_screen";
    public static final String NOTE_TYPE = "NOTE_TYPE";
    public static final String NOTE_GUID = "NOTE_GUID";
}
