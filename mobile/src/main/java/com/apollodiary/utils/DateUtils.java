package com.apollodiary.utils;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DateUtils {

    private static DateTime createDateTimeObject(Long milliseconds) {
        return milliseconds == null ? new DateTime() : new DateTime(milliseconds);
    }

    public static String getDateTimeString(String pattern) {
        DateTime dt = new DateTime();
        DateTimeFormatter fmt = DateTimeFormat.forPattern(pattern);
        return fmt.print(dt);
    }

    public static String getDateTimeStringFromMillis(long millis, String pattern) {
        DateTime dt = new DateTime(millis);
        DateTimeFormatter fmt = DateTimeFormat.forPattern(pattern);
        return fmt.print(dt);
    }

    public static String getDayOfWeekShort() {
        return getDayOfWeekShort(null);
    }

    public static String getDayOfWeekShort(Long milliseconds) {
        DateTime dt = createDateTimeObject(milliseconds);
        return dt.dayOfWeek().getAsShortText();
    }

    public static String getDayOfWeekLong() {
        return getDayOfWeekLong(null);
    }

    public static String getDayOfWeekLong(Long milliseconds) {
        DateTime dt = createDateTimeObject(milliseconds);
        return dt.dayOfWeek().getAsText();
    }

    public static String getDayOfMonth() {
        return getDayOfMonth(null);
    }

    public static String getDayOfMonth(Long milliseconds) {
        DateTime dt = createDateTimeObject(milliseconds);
        return dt.dayOfMonth().getAsText();
    }

    public static String getMonthOfYearShort() {
        return getMonthOfYearShort(null);
    }

    public static String getMonthOfYearShort(Long milliseconds) {
        DateTime dt = createDateTimeObject(milliseconds);
        return dt.monthOfYear().getAsShortText();
    }

    public static String getMonthOfYearLong() {
        return getMonthOfYearLong(null);
    }

    public static String getMonthOfYearLong(Long milliseconds) {
        DateTime dt = createDateTimeObject(milliseconds);
        return dt.monthOfYear().getAsText();
    }

    public static String getYearShort() {
        return getYearShort(null);
    }

    public static String getYearShort(Long milliseconds) {
        DateTime dt = createDateTimeObject(milliseconds);
        return dt.year().getAsShortText();
    }

    public static String getYearLong() {
        return getYearLong(null);
    }

    public static String getYearLong(Long milliseconds) {
        DateTime dt = createDateTimeObject(milliseconds);
        return dt.year().getAsText();
    }

    public enum DateFormat {
        TIME_FORMAT_24HR("kk:mm");

        public String formatPattern;

        DateFormat(String formatPattern) {
            this.formatPattern = formatPattern;
        }

    }
}
