package com.apollodiary;


import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import de.greenrobot.event.EventBus;

public abstract class EvernoteActivity extends ActionBarActivity {

    protected EventBus eventBus;

    protected boolean isNeedRegister = false;

    protected abstract void setEventBusRegister();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eventBus = ApolloDiaryApplication.getEventBus();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isNeedRegister){
            eventBus.register(this);
        }
    }

    @Override
    protected void onPause() {
        if(isNeedRegister) {
            eventBus.unregister(this);
        }
        super.onPause();
    }


}
